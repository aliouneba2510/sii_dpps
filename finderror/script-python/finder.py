import sys
import json
import unicodedata

def logger(info=None):
    if info == None:
        print("************************************************************************")
    else:
        print("*********************************** {} *************************************".format(info))

def log_error(info):
    print("*********************************** {} *************************************".format(info))

def find_error_linenumber():
    dictionary = {}
    first = {}
    next_lines_after_crash = []
    logger()
    with open(sys.argv[1], 'r') as jsonfile:
        list_ = list()
        d = json.load(jsonfile)
        for key, value in d.items():
            list_.append(value)
    with open(sys.argv[2], 'rb') as f:
        try:
            lines = f.readlines()
            line_counter = 0
            for line in lines:
                for tag in list_:
                    tag = unicodedata.normalize('NFKD', tag).encode('ascii', 'ignore')
                    if tag.lower() in line.lower():
                        dictionary, first = add_in_dictionary(dictionary, first, line.decode('utf-8'), "{}".format(tag.decode('utf-8')))
                        logger()
                        print('Error : "{}" found'.format(tag.decode('utf-8')))
                        print(line.decode('utf-8'))
                        logger()
                        if tag.decode('utf-8') == "beginning of crash":
                            next_lines_after_crash = add_next_lines_after_crash(next_lines_after_crash, line_counter, lines)
                line_counter += 1
        except UnicodeDecodeError as e:
           print('Offset: {}, {}'.format(e.start, e.reason))
    logger()
    log_error_count(dictionary, first, next_lines_after_crash)

def add_in_dictionary(dictionary, first, line, tag):
    keys = dictionary.keys()
    if tag in keys:
        dictionary[tag] = dictionary[tag] + 1
    else:
        dictionary[tag] = 1
        first[tag] = line    
    return dictionary, first

def add_next_lines_after_crash(next_lines, line_counter, lines):
    for i in range(line_counter+1, line_counter+71):
        next_lines.append(lines[i].decode('utf-8'))
    return next_lines

def log_error_count(dictionary, first, next_lines):
    log_error("SYNTHESIS")
    for tag in dictionary.keys():
        print("Error : {} occured {} times".format(tag, dictionary[tag]))
        print("{}".format(first[tag]))
        if tag == "beginning of crash":
            for line in next_lines:
                print(line)
        logger()

log_error("BEGINNING")
find_error_linenumber()
log_error("END")
