olddir=$(ls)

wget $1 --no-check-certificate

newdir=$(ls)

function list_not_include_item() {
    for i in $newdir
    do
        local list=$olddir
        local item=$i
        if !([[ $list =~ (^|[[:space:]])$item($|[[:space:]]) ]])
        then
	    newfile=$item
            break
        fi
    done
}

list_not_include_item
cd script-python
python3 finder.py tags.json ../$newfile
rm ../$newfile

