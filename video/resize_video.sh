if ! command -v ffmpeg &> /dev/null
then
    echo "Install ffmpeg"
    echo "Run the following command (sudo apt-get install ffmpeg -y)"
    exit
fi

folder=$1
video=$2
number=$3
cd $folder
echo $video
seconds=$(ffprobe -i $video -show_entries format=duration -v quiet -of csv="p=0")

seconds=$(echo $seconds | tr "." "\n")
video__=$(echo $video | tr "." "\n")

indicator=0
indic=0
for second in $seconds
do
    if [ $indicator -eq 0 ]
    then
        second_first=$second
    	indicator="1"
    else
        second_last=$second
    fi
done

for vv in $video__
do
    if [ $indic -eq 0 ]
    then
        video__=$vv
    	indic="1"
    fi
done

hours=$(echo $second_first/3600 | bc)
rest_hours=$(echo $second_first%3600 | bc)
minutes=$(echo $rest_hours/60 | bc)
seconds=$(echo $rest_hours%60 | bc)

for i in `seq 1 $number`
do
    echo $i
    hour=$(echo $hours/$number | bc)
    minute=$(echo $minutes/$number | bc)
    second=$(echo $seconds/$number | bc)
    before=$(echo $i-1 | bc)
    hb=0
    mb=0
    sb=0
    ms="00000"
    if [ $before -gt 0 ]
    then
        hb=$(echo $hour*$before | bc)
        mb=$(echo $minute*$before | bc)
        sb=$(echo $second*$before | bc)
    fi
    if [ $i -eq $2 ]
    then
	hour_=$(echo $hours%$number | bc)
        minute_=$(echo $minutes%$number | bc)
        second_=$(echo $seconds%$number | bc)
        hour=$(echo $hour+$hour_ | bc)
        minute=$(echo $minute+$minute_ | bc)
        second=$(echo $second+$second_ | bc)
	ms=$second_last
    fi
    if [ $hour -lt 10 ]
    then
        hour="0$hour"
    fi
    if [ $hb -lt 10 ]
    then
        hb="0$hb"
    fi
    if [ $minute -lt 10 ]
    then
        minute="0$minute"
    fi
    if [ $mb -lt 10 ]
    then
        mb="0$mb"
    fi
    if [ $second -lt 10 ]
    then
        second="0$second"
    fi
    if [ $sb -lt 10 ]
    then
        sb="0$sb"
    fi
    ffmpeg -i $video -ss $hb:$mb:$sb.00 -t $hour:$minute:$second.$ms -c:v copy -c:a copy "$video__"_$i.mp4
done
rm $video

